-- Add 5 Artists

INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");

-- Add albums to TS:
INSERT INTO albums(album_title, date_released, artist_id) 
VALUES ('Fearless', "2008-1-1", 3);

		--  Add songs to Fearless Album:
		INSERT INTO songs (song_name, duration, genre, album_id) 
		VALUES ('Fearless', 246, 'Pop Rock', 4);

		INSERT INTO songs (song_name, duration, genre, album_id) 
		VALUES ('Love Story', 213, 'Country Pop', 4);

INSERT INTO albums(album_title, date_released, artist_id) 
VALUES ('Red', "2012-1-1", 3);

		--  Add songs to Red Album:
		INSERT INTO songs (song_name, duration, genre, album_id) 
		VALUES ("State of Grace", 249, "Rock, Alternative rock, Arena Rock", 5);

		INSERT INTO songs (song_name, duration, genre, album_id) 
		VALUES ("Red", 204, "Country", 5);


-- Add albums to Lady Gaga:
INSERT INTO albums(album_title, date_released, artist_id) 
VALUES ('A Star Is Born', "2018-1-1", 4);

		--  Add songs to A Star Is Born Album:
		INSERT INTO songs (song_name, duration, genre, album_id) 
		VALUES ("Black Eyes", 151, "Rock and roll", 6);
		
		INSERT INTO songs (song_name, duration, genre, album_id) 
		VALUES ("Shallow", 201, "Country, rock, folk rock", 6);


INSERT INTO albums(album_title, date_released, artist_id) 
VALUES ('Born This Way', "2011-1-1", 4);

		INSERT INTO songs (song_name, duration, genre, album_id) 
		VALUES ("Born This Way", 252, "Electropop", 7);

-- Add albums to Justin Bieber:
INSERT INTO albums (album_title, date_released, artist_id) 
VALUES ("Purpose", "2015-1-1", 5);
	
		INSERT INTO songs (song_name, duration, genre, album_id) 
		VALUES ("Sorry", 142, "Dancehall-poptropical housemoombahton", 8);

-- Add albums to Justin Bieber:
INSERT INTO albums (album_title, date_released, artist_id) 
VALUES ("Believe", "2012-1-1", 5);

		INSERT INTO songs (song_name, duration, genre, album_id) 
		VALUES ("Boyfriend", 251, "Pop", 9);

-- Add albums to Arian Grande:
INSERT INTO albums (album_title, date_released, artist_id) 
VALUES ("Dangerous Woman", "2016-1-1", 6);

		INSERT INTO songs (song_name, duration, genre, album_id) 
		VALUES ("Into You", 242, "EDM house", 10);

INSERT INTO albums (album_title, date_released, artist_id) 
VALUES ("Thank U, Next", "2019-1-1", 6);

		INSERT INTO songs (song_name, duration, genre, album_id) 
		VALUES ("Thank U Next", 156, "Pop, R&B", 11);


-- Add albums to Bruno Mars:
INSERT INTO albums (album_title, date_released, artist_id) 
VALUES ("24K Magic", "2016-1-1", 7);

		INSERT INTO songs (song_name, duration, genre, album_id) 
		VALUES ("24K Magic", 207, "Funk, disco, R&B", 12);


INSERT INTO albums (album_title, date_released, artist_id) 
VALUES ("Earth to Mars", "2011-1-1", 7);
		
		INSERT INTO songs (song_name, duration, genre, album_id) 
		VALUES ("Lost", 152, "Pop", 13);



-- [ SECTION ] ADVANCED Selects:

-- Exclude records:
SELECT * FROM songs WHERE id != 11;

SELECT song_name, genre FROM songs WHERE id != 11;

-- Greater than or equal operator:
SELECT * FROM songs WHERE id >= 11;

-- Less than or equal operator
SELECT * FROM songs WHERE id <= 11;

-- What if  want those records with ID from 5 to 11;
SELECT * FROM songs WHERE id >= 5 AND <= 11
	-- Another way:
	SELECT * FROM songs WHERE id BETWEEN 5 AND 11;

-- GET specific IDs
SELECT * FROM songs WHERE id = 11;

-- OR Operator;
SELECT * FROM songs WHERE id = 1 OR id = 11 OR id = 5;

-- IN Operator
SELECT * FROM songs WHERE id IN (1, 11, 5);

-- Find PARTIAL Matches:
-- ends with letter e
SELECT * FROM songs WHERE song_name LIKE "%e";
-- starts with letter s
SELECT * FROM songs WHERE song_name LIKE "s%";

-- records with album id with 10
SELECT* FROM songs WHERE album_id LIKE "10_";


-- SORT Rcord
-- Ascending
SELECT * FROM songs ORDER BY album_id ASC;

-- Descending
SELECT * FROM songs ORDER BY song_name DESC;

-- Distinct 
-- This part of the query specifies that you are selecting records from the songs table.
SELECT DISTINCT genre FROM songs;


-- [ SECTION ] Table joins:
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id;


-- Join albums table and songs table
SELECT * FROM albums JOIN songs ON albums.id = songs.album_id;


/*
	In the query below:

	albums.album_title and songs.song_name are column selections within the SELECT clause.
	albums is the primary table from which you're selecting data.
	JOIN songs ON albums.id = songs.album_id specifies the join condition between the albums and songs tables.
*/

SELECT albums.album_title, songs.song_name, songs.duration
FROM albums
JOIN songs ON albums.id = songs.album_id;


SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;


SELECT artists.name, albums.album_title, songs.song_name FROM artists
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;









